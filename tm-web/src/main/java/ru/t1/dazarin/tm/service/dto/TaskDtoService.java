package ru.t1.dazarin.tm.service.dto;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.dazarin.tm.api.repository.ITaskDtoRepository;
import ru.t1.dazarin.tm.exception.entity.TaskNotFoundException;
import ru.t1.dazarin.tm.model.dto.TaskDto;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TaskDtoService {

    private final ITaskDtoRepository taskDtoRepository;

    public List<TaskDto> findAll() {
        return taskDtoRepository.findAll();
    }

    @Nullable
    public TaskDto findById(@NotNull final String id) {
        return taskDtoRepository.findById(id).orElse(null);
    }

    public TaskDto create() {
        @NotNull final TaskDto taskDto = new TaskDto();
        taskDto.setName("Task Name " + System.currentTimeMillis());
        taskDto.setDescription("Task Description");
        return taskDtoRepository.saveAndFlush(taskDto);
    }

    public void deleteById(@NotNull final String taskDtoId) {
        if (!taskDtoRepository.existsById(taskDtoId)) throw new TaskNotFoundException();
        taskDtoRepository.deleteById(taskDtoId);
    }

    public void deleteAll() {
        taskDtoRepository.deleteAll();
    }

    public TaskDto save(@NotNull final TaskDto taskDto) {
        return taskDtoRepository.save(taskDto);
    }

}
