package ru.t1.dazarin.tm.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.dazarin.tm.model.dto.ProjectDto;

@Repository
public interface IProjectDtoRepository extends JpaRepository<ProjectDto, String> {
}
