package ru.t1.dazarin.tm.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.dazarin.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_project")
public final class ProjectDto extends AbstractDtoModel {

    @Column
    @NotNull
    private String name = "";

    @Column
    @NotNull
    private String description = "";

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @CreatedDate
    @Column(name = "created_at")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date createdDate = new Date();

    @Column(name = "date_start")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateStart;

    @Column(name = "date_finish")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish;

    public ProjectDto(@NotNull final String name) {
        this.name = name;
    }

    public ProjectDto(@NotNull final String name, @NotNull final String description) {
        this.name = name;
        this.description = description;
    }

}
