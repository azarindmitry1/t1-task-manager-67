package ru.t1.dazarin.tm.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("ru.t1.dazarin.tm")
public class ApplicationConfiguration {
}
