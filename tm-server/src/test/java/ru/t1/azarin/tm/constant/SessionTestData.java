package ru.t1.azarin.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.dto.model.SessionDto;
import ru.t1.azarin.tm.enumerated.Role;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@UtilityClass
public final class SessionTestData {

    @NotNull
    public final static SessionDto USER1_SESSION1 = new SessionDto();

    @NotNull
    public final static SessionDto ADMIN_SESSION1 = new SessionDto();

    @NotNull
    public final static List<SessionDto> USER1_SESSION_LIST = Arrays.asList(USER1_SESSION1);

    @NotNull
    public final static List<SessionDto> ADMIN_SESSION_LIST = Arrays.asList(ADMIN_SESSION1);

    @NotNull
    public final static List<SessionDto> SESSION_LIST = new ArrayList<>();

    static {
        USER1_SESSION1.setRole(Role.USUAL);
        USER1_SESSION1.setDate(new Date());
    }

}
