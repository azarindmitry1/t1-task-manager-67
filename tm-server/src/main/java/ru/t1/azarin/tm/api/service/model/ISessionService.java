package ru.t1.azarin.tm.api.service.model;

import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.model.Session;

import java.util.List;

public interface ISessionService {

    void add(@Nullable Session model);

    void clear(@Nullable String userId);

    @Nullable
    List<Session> findAll(@Nullable String userId);

    @Nullable
    Session findOneById(@Nullable String userId, @Nullable String id);

    void remove(@Nullable Session model);

    void removeById(@Nullable String userId, @Nullable String id);

    Session update(@Nullable Session model);

}
