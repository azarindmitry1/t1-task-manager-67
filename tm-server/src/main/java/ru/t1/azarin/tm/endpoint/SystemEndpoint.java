package ru.t1.azarin.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.azarin.tm.api.endpoint.ISystemEndpoint;
import ru.t1.azarin.tm.api.service.IPropertyService;
import ru.t1.azarin.tm.dto.request.system.ApplicationAboutRequest;
import ru.t1.azarin.tm.dto.request.system.ApplicationVersionRequest;
import ru.t1.azarin.tm.dto.response.system.ApplicationAboutResponse;
import ru.t1.azarin.tm.dto.response.system.ApplicationVersionResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@NoArgsConstructor
@AllArgsConstructor
@WebService(endpointInterface = "ru.t1.azarin.tm.api.endpoint.ISystemEndpoint")
public final class SystemEndpoint extends AbstractEndpoint implements ISystemEndpoint {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Override
    @WebMethod
    public ApplicationAboutResponse getAbout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ApplicationAboutRequest request
    ) {
        @NotNull final ApplicationAboutResponse response = new ApplicationAboutResponse();
        response.setName(propertyService.getAuthorName());
        response.setEmail(propertyService.getAuthorEmail());
        return response;
    }

    @NotNull
    @Override
    @WebMethod
    public ApplicationVersionResponse getVersion(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ApplicationVersionRequest request
    ) {
        @NotNull final ApplicationVersionResponse response = new ApplicationVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }

}
