package ru.t1.azarin.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.azarin.tm.dto.model.ProjectDto;

import java.util.List;

@Repository
@Scope("prototype")
public interface ProjectDtoRepository extends AbstractUserOwnedDtoRepository<ProjectDto> {

    void deleteAllByUserId(@Nullable String userId);

    @Nullable
    List<ProjectDto> findAllByUserId(@NotNull String userId);

    @Query("SELECT p FROM ProjectDto p WHERE p.userId = :userId ORDER BY :sortColumn")
    List<ProjectDto> findAllByUserIdWithSort(@Param("userId") @NotNull String userId, @Param("sortColumn") @NotNull String sortColumn);

    @Nullable
    ProjectDto findByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

}
