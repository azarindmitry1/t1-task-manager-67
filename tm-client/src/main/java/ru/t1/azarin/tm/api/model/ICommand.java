package ru.t1.azarin.tm.api.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.event.ConsoleEvent;

import java.sql.SQLException;

public interface ICommand {

    void handler(@NotNull ConsoleEvent consoleEvent) throws SQLException;

    @Nullable
    String getName();

    @Nullable
    String getArgument();

    @Nullable
    String getDescription();

}
